Ce dossier contient les scripts et documents permettant d'importer plusieurs fichiers TEI dans Nakala en s'appuyrant sur l'API. Un dossier par type de corpus sera créé pour rassembler les scripts permettant de :
1. Ajouter un `<xenodata>` aux `<teiHeader>` des fichiers à déposer
2. Créer un fichier CSV à partir des fichiers à déposer
3. Intéragir avec l'API pour déposer les fichiers dans Nakala
4. Récupérer les informations du dépôt et les DOI associés
5. Injecter les DOI dans les fichiers TEI déposés
