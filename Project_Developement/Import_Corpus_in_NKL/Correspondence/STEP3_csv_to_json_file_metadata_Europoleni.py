"""
- author: Gwenaëlle Patat
- date: December 2021
- description: Importing metadata from a csv file into Nakala with the API
- input: csv file
- output: data imported into Nakala
- source code : https://gitlab.huma-num.fr/huma-num-public/notebook-api-nakala
"""

# Sending data to the API and creation of a log file : final script of batch import
# NB : It is advisable to have created collection(s) will receive data upstream of data repository, and the lists of users too. 

import csv, requests, json, time

# Endpoint API & API key
apiUrl = 'https://apitest.nakala.fr'
# user test unakala3
apiKey = 'aae99aba-476e-4ff2-2886-0aaf1bfa6fd2'

# 1.1 Reading of a csv file
with open('meta_Europoleni_Nakala_cleaned.csv', newline='') as f:
    reader = csv.reader(f)
    dataset = list(reader)
dataset.pop(0)

# 1.2 Preparation of an output file
output = open('output_data_nkl.csv', 'w') # opening of a file in writing mode
outputWriter = csv.writer(output) # creation of an object to write in this file
header = ['identifier', 'files', 'title', 'status', 'response'] # column names to insert in the file
outputWriter.writerow(header) # witing of column names in the file

# 2. Loop for considering all the lines of the file
for num, data in enumerate(dataset):
    
    # 2.1. Récupération of available information on the data to create
    filenames = data[0]
    status = data[1]
    collection = data[2]
    datatype = data[3]
    license = data[4]
    publisher = data[5]
    datarights = data[6].split(';')
    title = data[7]
    contributors = data[8].split(';')
    source = data[9]
    description = data[10].split(';')
    biblio = data[11].split(';')
    author = data[12]
    date = data[13]
    audience = data[14]
    keywords = data[15].split(';')
    language = data[16]
    
    outputData = ['','',title,'','']; # Variable where will be stored information to write in the output file
    
    print('CREATION DE LA DONNEE ' + str(num) + " : " + title)
    
    # 2.2. Sending files to the API
    files = []
    outputFiles = [] # Variable to store information to write in the output file concerning upload files
    #for filename in filenames:
    goToNextData = False # Variable to know if we must pass to the next data in case of error in an upload
    print('Envoi du fichier ' + filenames + '...')
    payload={}
    # Precising MIME type in the imported file
    postfiles=[('file',(filenames,open('./XML_files/' + filenames, 'rb'),'text/xml'))]
    headers = {'X-API-KEY': apiKey }
    response = requests.request('POST', apiUrl + '/datas/uploads', headers=headers, data=payload, files=postfiles)
    if ( 201 == response.status_code ):
        file = json.loads(response.text)
        file["embargoed"] = time.strftime("%Y-%m-%d") # We indicate current date
        files.append(file)
        # Adding name file and sha1 to the information to write in the output file
        outputFiles.append(filenames + ',' + file["sha1"])
    else:
        # An error is happened in the upload
        outputFiles.append(filenames)
        outputData[1] = ';'.join(outputFiles)
        outputData[3] = 'ERROR'
        outputData[4] = response.text
        outputWriter.writerow(outputData)
        print ("Certains fichiers n'ont pas pu être envoyés, on passe à la donnée suivante...")
        goToNextData = True # we indicate in this variable that we must pass to next data
        break # we stopp the upload of the other files of the data
        if goToNextData: continue # we pass to next data if there is an error
    
    # 2.3. We keep a trace of the list of uploaded files in the output file.
    outputData[1] = ';'.join(outputFiles)
    
    # 2.4. Metadata reconstruction
    metas = []
    
        # Type metadata (mandatory)
    metaType = {
        "value": datatype, # Insertion of the content of "datatype" column (just one type by data)
        "typeUri": "http://www.w3.org/2001/XMLSchema#anyURI", # The value filled is a URI
        "propertyUri": "http://nakala.fr/terms#type" # Field "type" from Nakala's vocabulary
    }
    metas.append(metaType) # adding metadata in "metas" table
    
    # Title metadata (mandatory)
    metaTitle = {
        "value": title, # Insertion of the content of "title" column (just one title by data)
        "lang": "fr", # Language of the title (cf. norms ISO-639-1 or ISO-639-3 for unusual languages)
        "typeUri": "http://www.w3.org/2001/XMLSchema#string", # The value filled is a string
        "propertyUri": "http://nakala.fr/terms#title" # Field "title" from Nakala's vocabulary
    }
    metas.append(metaTitle)
    
    # Author(s) metadata (mandatory for a published data)
    #for author in authors: # La colonne "authors" peut comporter plusieurs valeurs qu'on parcours une à une
        # For each value, divise last name and first name to build the metadata nakala:creator
    surnameGivenname = author.split(',')
    metaAuthor = {
        "value": { # The value of this metadata is not a simple string, but an object composed at least by 2 properties (givenname and surname)
            "givenname": surnameGivenname[1], # first name
            "surname": surnameGivenname[0] # last name
        },
        "propertyUri": "http://nakala.fr/terms#creator" # Field "creator" from Nakala's vocabulary
    }
    metas.append(metaAuthor)
        
    # Date of creation metadata (mandatory for a published data)
    metaCreated = {
        "value": date, # Insertion of the content of "created" column (just one date by data)
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://nakala.fr/terms#created"
    }
    metas.append(metaCreated)
    
    # Licence metadata (mandatory for a published data)
    metaLicense = {
        "value": license, # Insertion of the content of "license" column (just one licence by data)
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://nakala.fr/terms#license"
    }
    metas.append(metaLicense)
    
    # Description metadata (optional)
    for description in description: # loop for considering all values in "description" column
        metaDescription = {
            "value": description, # Insertion of the content "description" column
            "lang": "fr",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://purl.org/dc/terms/description" 
        }
        metas.append(metaDescription)
    
    # Publisher metadata (optional)
    metaPublisher = {
        "value": publisher, # Insertion of the content "publisher" column
        "lang": "fr",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://purl.org/dc/terms/publisher"
    }
    metas.append(metaPublisher)
    
    # Keywords metadata (optional)
    for keyword in keywords: # loop for considering all values in "subject" column
        metaKeyword = {
            "value": keyword, # Insertion of the content "subject" column
            "lang": "fr",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://purl.org/dc/terms/subject" 
        }
        metas.append(metaKeyword)
        
    # Contributor metadata (optional)
    for contributor in contributors: # loop for considering all values in "contributor" column
        metaContributor = {
            "value": contributor, # Insertion of the content "contributor" column
            "lang": "fr",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://purl.org/dc/terms/contributor"
        }
        metas.append(metaContributor)
        
    # Source metadata (optional)
    metaSource = {
        "value": source, # Insertion of the content "source" column
        "lang": "fr",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://purl.org/dc/terms/source" # Notez qu'il ne s'agit plus ici d'une propriété issue du vocabulaire NAKALA, mais du vocabulaire Dcterms
    }
    metas.append(metaSource)
    
    # Bibliographic metadata (optional)
    for biblio in biblio: # loop for considering all values in "bibliographicCitation" column
        metaBiblio = {
            "value": biblio, # Insertion of the content "bibliographicCitation" column
            "lang": "fr",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://purl.org/dc/terms/bibliographicCitation"
        }
        metas.append(metaBiblio)
        
    # Audience metadata (optional)
    metaAudience = {
        "value": audience, # Insertion of the content "audience" column
        "lang": "fr",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://purl.org/dc/terms/audience"
    }
    metas.append(metaAudience)
    
    # Language metadata (optional)
    metaLanguage = {
        "value": language, # Insertion of the content "language" column
        "lang": "fr",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://purl.org/dc/terms/language"
    }
    metas.append(metaLanguage)
    
    # 2.5. Rights reconstruction
    rights = [] # Datarights storage in a table
    for dataright in datarights: # loop for considering all values in "datarights" column
        datarightSplit = dataright.split(',') # Each value is made up of an "id" and a "role" seperated by a comma.
        if len(datarightSplit) == 2: # We verify we have only 2 values
            right = { # We rebuild "right" object made of "id" and "role" properties
                "id": datarightSplit[0],
                "role": datarightSplit[1]
            }
            rights.append(right) # We add "right" object in "rights" table
            
    """collections = []  # Collections linked to a data storage
    collectionsIds = collection.split(",")
    if len(collectionsIds) >= 2: # We verify that we have more of 2 values
            collection = { # We rebuild "right" object made of "collection" and "collectionIds" properties
                "collectionIds": collectionsIds
            }"""
    
    # 3. Sending data to Nakala
    postdata = { # Variable containing request content
        "status" : status,
        "files" : files,
        "metas" : metas,
        #"rights": rights
    }
    content = json.dumps(postdata) # Serialization of the content in JSON
    headers = { # Header's request'
      'Content-Type': 'application/json',
      'X-API-KEY': apiKey,
    }
    response = requests.request('POST', apiUrl + '/datas', headers=headers, data=content) # Call of the API
    if ( 201 == response.status_code ): # 201 code if all goes well
        parsed = json.loads(response.text) # We parse API response
        print('La donnée ' + str(num) + ' a bien été créée : ' + parsed["payload"]["id"] + '\n') # Display of a message of success
        outputData[0] = parsed["payload"]["id"] # We store information needed in the output file
        outputData[3] = 'OK'
        outputData[4] = response.text
    else:
        print("Une erreur s'est produite !") # In case of an error, we display a message
        outputData[3] = 'ERROR' # we complete needed information for the output file
        outputData[4] = response.text
    outputWriter.writerow(outputData) # We write information in the output file

# 4. Closing of the output file
output.close()